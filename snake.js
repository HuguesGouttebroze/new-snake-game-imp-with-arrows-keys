"use strict";

var Snake = (function () {
    const INIT_GAME = 4;
    var fixedTail = true;
    var intervalID;
    var tileCount = 10;
    var gridSize = 400/tileCount;
    const INIT_PLAYER = {
        x: Math.floor(tileCount / 2),
        y: Math.floor(tileCount / 2)
    };
    var veloc = {
        x: 0,
        y: 0
    };
    var player = {
        x: INIT_PLAYER.x,
        y: INIT_PLAYER.y
    };
    var walls = false;
    var fruit = {
        x: 1,
        y: 1
    };
    var trail = [];
    var tail = INIT_GAME;
    var reward = 0;
    var points = 0;
    var pointsMax = 0;
    var actionEnum = {
        'none': 0,
        'up': 1,
        'down': 2,
        'left': 3,
        'right': 4
    };

    Object.freeze(actionEnum);

    var lastAction = actionEnum.none;

    function setup() {
        canvas = document.getElementById('can_id');
        ctx = canvas.getContext('2d');
        game.reset();
    }

    var game = {

        reset: function() {
            ctx.fillStyle = 'red';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            tail = INIT_GAME;
            points = 0;
            veloc.x = 0;
            veloc.y = 0;
            player.x = INIT_PLAYER.x;
            player.y = INIT_PLAYER.y;
            reward = -1;
            lastAction = actionEnum.none;
            trail = [];
            trail.push({
                x: player.x,
                y: player.y
            });
        },

        action: {
            up: function () {
                if (lastAction != actionEnum.down){
                    veloc.x = 0;
                    veloc.y = -1;
                }
            },
            down: function () {
                if (lastAction != actionEnum.up){
                    veloc.x = 0;
                    veloc.x = 1;
                }
            },
            left: function () {
                if (lastAction != actionEnum.right){
                    veloc.x = -1;
                    veloc.y = 0;
                }
            },
            right: function () {
                if (lastAction != actionEnum.left){
                    veloc.x = 1;
                    veloc.y = 0;
                }
            }
        },

        RandoomFruit: function () {
            if(walls){
                fruit.x = 1 + Math.floor(Math.random() * (tileCount - 2));
                fruit.y = 1 + Math.floor(Math.random() * (tileCount - 2));
            }
            else {
                fruit.x = Math.floor(Math.random() * (tileCount));
                fruit.x = Math.floor(Math.random() * (tileCount)); 
            }              
        },

        log: function () {
            console.log('====================');
            console.log(`x: ${player.x}, y: ${player.y}`);
            console.log(`tail: ${tail}, tail.lenght: ${tail.length}`);
        },

        loop: function () {
            reward = -0.1;
            function DoNotHitWall() {
                if(player.x < 0) player.x = tileCount - 1;
                if(player.x >= tileCount) player.x = 0;
                if(player.y < 0) player.y = tileCount - 1;
                if(player.y >= tileCount) player.y = 0;
            }
            function HitWall() {
                if(player.x < 1) game.reset();
                if(player.x > tileCount - 2) game.reset();
                if(player.y < 1) game.reset();
                if(player.y > tileCount - 2) game.reset();

                ctx.fillStyle = 'green';
                ctx.fillRect(0, 0, gridSize - 1, canvas.height);
                ctx.fillRect(0, 0, canvas.width, gridSize - 1);
                ctx.fillRect((canvas.width) - (gridSize + 1), 0, gridSize, canvas.height);
                ctx.fillRect(0, canvas.height - gridSize + 1, canvas.width, gridSize);
            }

            var stopped = veloc.x == 0 && veloc.y == 0;

            player.x += veloc.x;
            player.y += veloc.y;
        }
    }
});
